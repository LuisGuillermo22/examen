package compruebamovil.molinaalmeida.facci.prueba;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText numpasaje, avion, fecha, nombrepasajero, origen, id;
    private Button guardar, leer, leerT, actualizar, Eliminar, eliminarT;
    private TextView Datos;
    private Helper dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numpasaje = (EditText)findViewById(R.id.numpasaje);
        avion = (EditText)findViewById(R.id.avion);
        fecha = (EditText)findViewById(R.id.fecha);
        nombrepasajero = (EditText)findViewById(R.id.numpasaje);
        origen = (EditText)findViewById(R.id.origen);
        id = (EditText)findViewById(R.id.Id);
        guardar = (Button)findViewById(R.id.Guardar);
        leerT = (Button)findViewById(R.id.LeerT);
        actualizar = (Button)findViewById(R.id.Actualizar);
        eliminarT = (Button)findViewById(R.id.EliminarT);
        leer = (Button)findViewById(R.id.leerin);
        guardar.setOnClickListener(this);
        leerT.setOnClickListener(this);
        leer.setOnClickListener(this);

        actualizar.setOnClickListener(this);
        eliminarT.setOnClickListener(this);
        Datos = (TextView)findViewById(R.id.Datos);
        dataBase = new Helper(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.Guardar:

                if (numpasaje.getText().toString().isEmpty()){

                }else if(numpasaje.getText().toString().isEmpty()){

                }else if(avion.getText().toString().isEmpty()){

                }else if (fecha.getText().toString().isEmpty()){

                }else if(nombrepasajero.getText().toString().isEmpty()){

                }else if(origen.getText().toString().isEmpty()){

                }else {
                    dataBase.Insertar(numpasaje.getText().toString(), avion.getText().toString(),
                            fecha.getText().toString(), nombrepasajero.getText().toString(), origen.getText().toString());
                    Toast.makeText(this, "GUARDADO", Toast.LENGTH_SHORT).show();
                    id.setText("");
                    numpasaje.setText("");
                    avion.setText("");
                    fecha.setText("");
                    nombrepasajero.setText("");
                    origen.setText("");
                    Datos.setText("");
                }

                break;

            case R.id.LeerT:
                Datos.setText(dataBase.LeerTodos());
                id.setText("");
                numpasaje.setText("");
                avion.setText("");
                fecha.setText("");
                nombrepasajero.setText("");
                origen.setText("");
                Toast.makeText(this, "LISTADOS", Toast.LENGTH_SHORT).show();
                break;

            case R.id.leerin:
                Datos.setText(dataBase.Leer(id.getText().toString()));
                id.setText("");
                numpasaje.setText("");
                avion.setText("");
                fecha.setText("");
                nombrepasajero.setText("");
                origen.setText("");
                Toast.makeText(this, "LISTADO", Toast.LENGTH_SHORT).show();

                break;


            case R.id.Actualizar:
                if (numpasaje.getText().toString().isEmpty()){

                }else if(numpasaje.getText().toString().isEmpty()){

                }else if(avion.getText().toString().isEmpty()){

                }else if (fecha.getText().toString().isEmpty()){

                }else if(nombrepasajero.getText().toString().isEmpty()){

                }else if(origen.getText().toString().isEmpty()){

                }else if(id.getText().toString().isEmpty()){

                }else {
                    dataBase.Actualizar(id.getText().toString(), numpasaje.getText().toString(), avion.getText().toString(),
                            fecha.getText().toString(), nombrepasajero.getText().toString(), origen.getText().toString());
                    Toast.makeText(this, "ACTUALIZADO", Toast.LENGTH_SHORT).show();
                    id.setText("");
                    numpasaje.setText("");
                    avion.setText("");
                    fecha.setText("");
                    nombrepasajero.setText("");
                    origen.setText("");
                    Datos.setText("");
                }
                break;

            case R.id.EliminarT:
                dataBase.EliminarTodo();
                Datos.setText("");
                Toast.makeText(this, "ELIMINADOS", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}